#ifndef APPSH_H
# define APPSH_H

//#include <systemd/sd-bus.h>
#include "sds.h"

typedef enum {
	TXT,
	PNG,
	JPEG,
	PDF
}			format;

typedef struct bus_conn bus_conn;

bus_conn	*init_app_on_bus(const char *app_name);
int		add_format(bus_conn *conn, format F);
int		rm_format(bus_conn *conn, format F);
int		get_sharing_capable_apps(bus_conn *conn, format F, sds **mas);
int		close_connection(bus_conn **conn);
// share txt
// bind func on txt

#endif
