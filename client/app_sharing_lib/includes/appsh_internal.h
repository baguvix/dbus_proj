#ifndef APPSH_INTERNAL_H
# define APPSH_INTERNAL_H

#include <stdlib.h>
#include <systemd/sd-bus.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include "sds.h"

struct	bus_conn {
	sd_bus_error	error;
	sd_bus		*bus;
	sd_bus_slot	*slot;
	void		*userdata;
	sds		an;
	pthread_t	t;
};

#include "appsh.h"

typedef struct {
	void	*arg;
	void	(*txt_handler)(const char *txt, void *);
}			user_callback;

const char	*get_format_str(format F);
void		*listner(void *ar);
int		receive_txt(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);

static const sd_bus_vtable	share_vtab[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("Receive_txt", "s",  "s", receive_txt,   SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END
};

#endif
