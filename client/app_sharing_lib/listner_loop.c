#include "appsh_internal.h"

void	*listner(void *ar) {
	bus_conn	*c = (bus_conn *)(ar);
	int		r;

	while (1) {
		r = sd_bus_process(c->bus, NULL);
		if (r < 0) {
			fprintf(stderr, "Client failed to process bus: %s\n", strerror(-r));
			break;
		}
		if (r > 0)
			continue;

		r = sd_bus_wait(c->bus, (uint64_t) -1);
		if (r < 0) {
			fprintf(stderr, "Client failed to wait on bus: %s\n", strerror(-r));
			break;
		}
	}
	exit(1);
}
