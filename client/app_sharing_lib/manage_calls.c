#include "appsh_internal.h"

bus_conn	*init_app_on_bus(const char *app_name) {
	bus_conn	*new_c;
	sd_bus_message	*m = NULL;
	sds		opath, exec_path, iface;
	const char	*reply;
	char		tmp[PATH_MAX];
	int		r;

	if ((new_c = (bus_conn *)malloc(sizeof(bus_conn))) == NULL)
		return (NULL);

	new_c->error = SD_BUS_ERROR_NULL;
	new_c->bus   = NULL;
	new_c->slot  = NULL;
	new_c->userdata = NULL;

	new_c->an = sdsnew("ru.unknown.AppSharing.");
	new_c->an = sdscat(new_c->an, app_name);

	iface = sdsnew("ru.unknown.AppSharing.");
	iface = sdscat(iface, app_name);
	iface = sdscat(iface, ".Receive");

	getcwd(tmp, PATH_MAX);
	exec_path = sdsnew(tmp);
	exec_path = sdscat(exec_path, "/");
	exec_path = sdscat(exec_path, app_name);

	opath = sdsnew("/ru/unknown/AppSharing/");
	opath = sdscat(opath, app_name);

	r = sd_bus_open_user(&new_c->bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to user bus: %s\n", strerror(-r));
		return (NULL);
	}

	r = sd_bus_add_object_vtable(new_c->bus,
				     &new_c->slot,
				     opath,
				     iface,
				     share_vtab,
				     NULL);
	if (r < 0) {
		fprintf(stderr, "Failed to issue method call 1: %s\n", strerror(-r));
		return (NULL);
	}

	r = sd_bus_request_name(new_c->bus, new_c->an, 0);
	if (r == 0) {
		fprintf(stderr, "App registration failed: unic name required.\n");
		return (NULL);
	} else if (r < 0) {
		fprintf(stderr, "Name capture failed: %s\n", strerror(-r));
		return (NULL);
	}

	printf("%s\n%s\n", new_c->an, exec_path);
	r = sd_bus_call_method(new_c->bus,
			       "ru.unknown.AppSharing",
			       "/ru/unknown/AppSharing",
			       "ru.unknown.AppSharing.AppManager",
			       "App_auth",
			       &new_c->error,
			       &m,
			       "ss",
			       new_c->an,
			       exec_path);
	sdsfree(opath);
	sdsfree(iface);
        sdsfree(exec_path);
	if (r < 0) {
                fprintf(stderr, "Failed to issue method call 2: %s\n", new_c->error.message);
		return (NULL);
        }

        r = sd_bus_message_read(m, "s", &reply);
        if (r < 0) {
                fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
                return (NULL);
        }
	if (strcmp(reply, "SUCCESS") != 0)
		return (NULL);

	sd_bus_message_unref(m);

	pthread_create(&new_c->t, NULL, listner, (void *)(new_c));
	pthread_detach(new_c->t);

	return (new_c);
}

int	add_format(bus_conn *conn, format F) {
	sd_bus_message	*m = NULL;
	const char	*reply;
	int		r;

	r = sd_bus_call_method(conn->bus,
			       "ru.unknown.AppSharing",
			       "/ru/unknown/AppSharing",
			       "ru.unknown.AppSharing.AppManager",
			       "App_add_format",
			       &conn->error,
			       &m,
			       "ss",
			       conn->an,
			       get_format_str(F));
        if (r < 0) {
                fprintf(stderr, "Client failed to issue method call: %s\n", conn->error.message);
		return (-1);
        }

        /* Parse the response message */
        r = sd_bus_message_read(m, "s", &reply);
        if (r < 0) {
                fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
                return (-1);
        }
	if (strcmp(reply, "SUCCESS") != 0)
		return (-1);

	sd_bus_message_unref(m);
	return (0);
}

int	rm_format(bus_conn *conn, format F) {
	sd_bus_message	*m = NULL;
	const char	*reply;
	int		r;

	r = sd_bus_call_method(conn->bus,
			       "ru.unknown.AppSharing",
			       "/ru/unknown/AppSharing",
			       "ru.unknown.AppSharing.AppManager",
			       "App_rm_format",
			       &conn->error,
			       &m,
			       "ss",
			       conn->an,
			       get_format_str(F));
        if (r < 0) {
                fprintf(stderr, "Failed to issue method call: %s\n", conn->error.message);
		return (-1);
        }

        /* Parse the response message */
        r = sd_bus_message_read(m, "s", &reply);
        if (r < 0) {
                fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
                return (-1);
        }
	if (strcmp(reply, "SUCCESS") != 0)
		return (-1);

	sd_bus_message_unref(m);
	return (0);
}

int	get_sharing_capable_apps(bus_conn *conn, format F, sds **mas) {
	sd_bus_message	*m = NULL;
	const char	*reply;
	int		count = 0, r;

	r = sd_bus_call_method(conn->bus,
			       "ru.unknown.AppSharing",
			       "/ru/unknown/AppSharing",
			       "ru.unknown.AppSharing.AppManager",
			       "Format_match",
			       &conn->error,
			       &m,
			       "s",
			       get_format_str(F));
        if (r < 0) {
                fprintf(stderr, "Failed to issue method call: %s\n", conn->error.message);
		return (-1);
        }

        /* Parse the response message */
        r = sd_bus_message_read(m, "s", &reply);
        if (r < 0) {
                fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
                return (-1);
        }
	if (strcmp(reply, "FAILURE") == 0)
		return (-1);

	*mas = sdssplitlen(reply, strlen(reply), "|", 1, &count);

	sd_bus_message_unref(m);
	return (count);
}

int	close_connection(bus_conn **conn) {
	sd_bus_message	*m = NULL;
	bus_conn	*c = *conn;
	const char	*reply;
	int		r;

	r = sd_bus_call_method(c->bus,
			       "ru.unknown.AppSharing",
			       "/ru/unknown/AppSharing",
			       "ru.unknown.AppSharing.AppManager",
			       "App_down",
			       &c->error,
			       &m,
			       "s",
			       c->an);
        if (r < 0) {
                fprintf(stderr, "Failed to issue method call: %s\n", c->error.message);
		return (-1);
        }

        /* Parse the response message */
        r = sd_bus_message_read(m, "s", &reply);
        if (r < 0) {
                fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
                return (-1);
        }
	
	if (strcmp(reply, "FAILURE") == 0)
		return (-1);

	pthread_cancel(c->t);
	sdsfree(c->an);
	sd_bus_error_free(&c->error);
	sd_bus_message_unref(m);
	sd_bus_unref(c->bus);

	free(c);
	conn = NULL;
	return (0);
}

const char	*get_format_str(format F) {
	switch(F) {
	case TXT:
		return "txt";
	case PNG:
		return "png";
	case JPEG:
		return "jpeg";
	case PDF:
		return "pdf";
	default:
		return "BIG runtime safeness flaw...";
	}
}
