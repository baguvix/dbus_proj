#include "appsh_internal.h"

int	receive_txt(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	user_callback	*data = (user_callback *)(userdata);
	const char	*txt;
	int		r;

	r = sd_bus_message_read(m, "s", &txt);
	if (r < 0) {
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror (-r));
		return r;
	}

	data->txt_handler(txt, data->arg);

	return sd_bus_reply_method_return(m, "s", "SUCCESS");
}
