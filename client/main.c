#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include "appsh.h"

int	main(int argc, char **argv) {
	bus_conn	*connection;
	sds		*mas;
	int		r;

	connection = init_app_on_bus(basename(argv[0]));
	if (connection == NULL) {
		printf("Error1.. :c\n");
		exit(1);
	}

	r = add_format(connection, TXT);
	if (r < 0) {
		printf("Error2.. :c\n");
		exit(1);
	}
	
	r = get_sharing_capable_apps(connection, PNG, &mas);
	if (r < 0) {
		printf("Error3.. :c\n");
		exit(1);
	} else {
		int	count = 0;
		while (count < r) {
			printf("%d. %s\n", count + 1, mas[count]);
			++count;
		}
		printf("\n");
	}

	close_connection(&connection);
	exit(0);
}
