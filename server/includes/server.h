#ifndef APP_SHARING_SERV_H
# define APP_SHARING_SERV_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <systemd/sd-bus.h>
#include "sql_calls.h"

//typedef enum {
//	TXT,
//	PNG
//}		data_format;

typedef struct {
	sqlite3	*db;
	sds	str;
	sd_bus	*bus;
}		serv_data;

#endif
