#ifndef SQL_CALLS_H
# define SQL_CALLS_H

#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include "sds.h"

int	is_registered(sqlite3 *db, const char *app_name);
int	app_insert(sqlite3 *db, const char *app_name, const char *path);
int	format_add(sqlite3 *db, const char *app_name, const char *format);
int	format_rm(sqlite3 *db, const char *app_name, const char *format);
int	column_add(sqlite3 *db, const char *format);
int	format_request(sqlite3 *db, const char *format, sds *apps);
int	is_running(sqlite3 *db, const char *app_name);
int	wake_up_call(sqlite3 *db, const char *app_name);
int	app_shutdown(sqlite3 *db, const char *app_name);

#endif
