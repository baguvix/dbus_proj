#include "server.h"

static int	app_auth(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	const char	*app_name, *path;
	serv_data	*data = (serv_data *)(userdata);
	int		    r;

	r = sd_bus_message_read(m, "ss", &app_name, &path);
	if (r < 0){
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
		return r;
	}

	if (is_registered(data->db, app_name))
		printf("Already in!\n");
	else {
		if ((r = app_insert(data->db, app_name, path)) != 0)
			fprintf(stderr, "Faild to insert new app info: %s\n", sqlite3_errmsg(data->db));
		else
			fprintf(stderr, "Insertion successful!\n");
	}

	return sd_bus_reply_method_return(m, "s", "SUCCESS");
}

static int	app_add_format(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	const char	*app_name, *format;
	serv_data	*data = (serv_data *)(userdata);
	int		r;

	r = sd_bus_message_read(m, "ss", &app_name, &format);
	if (r < 0){
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
		return r;
	}

	if ((r = format_add(data->db, app_name, format)) != 0) {
		sd_bus_error_set_const(ret_error, "SQL Error", "Failed on format addition.");
		return sd_bus_reply_method_return(m, "s", "FAILURE");
	}

	return sd_bus_reply_method_return(m, "s", "SUCCESS");
}

static int	app_rm_format(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	const char	*app_name, *format;
	serv_data	*data = (serv_data *)(userdata);
	int		r;

	r = sd_bus_message_read(m, "ss", &app_name, &format);
	if (r < 0){
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
		return r;
	}

	if ((r = format_rm(data->db, app_name, format)) != 0) {
		sd_bus_error_set_const(ret_error, "SQL Error", "Failed on format remove.");
		return sd_bus_reply_method_return(m, "s", "FAILURE");
	}

	return sd_bus_reply_method_return(m, "s", "SUCCESS");
}

static int	format_match(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	const char	*format;
	serv_data	*data = (serv_data *)(userdata);
	int		r;

	r = sd_bus_message_read(m, "s", &format);
	if (r < 0){
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
		return r;
	}

	if ((r = format_request(data->db, format, &data->str)) != 0) {
		sd_bus_error_set_const(ret_error, "SQL Error", "Failed on apps sampling.");
		return sd_bus_reply_method_return(m, "s", "FAILURE");
	}

	return sd_bus_reply_method_return(m, "s", data->str);
}

static int	data_forward(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)	{
	const char      *app_name, *opath, *format, *fpath, *reply = NULL;
	serv_data       *data = (serv_data *)(userdata);
	sd_bus_message	*mes = NULL;
	sd_bus_error	error = SD_BUS_ERROR_NULL;
	sds		        method, iface;
	int		        r;

	r = sd_bus_message_read(m, "ssss", &app_name, &opath, &format, &fpath);
	if (r < 0){
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
		return r;
	}

	if (!is_running(data->db, app_name))
		if (!wake_up_call(data->db, app_name)) {
			sd_bus_error_set_const(ret_error, "SQL Error", "Failed on apps sampling.");
			return sd_bus_reply_method_return(m, "s", "FAILURE");
		}

	iface  = sdscatprintf(sdsempty(), "%s.Receive", app_name);
	method = sdscatprintf(sdsempty(), "Receive_%s", format);

	r = sd_bus_call_method(data->bus,
                            app_name,
                            opath,
                            iface,
                            method,
                            &error,
                            &mes,
                            "s",
                            fpath);
	if (r < 0) {
		fprintf(stderr, "Failed to issue method call: %s\n", error.message);
		goto exit;
	}

	r = sd_bus_message_read(mes, "s", &reply);
	if (r < 0) {
		fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
		goto exit;
	}
exit:
	sdsfree(iface);
	sdsfree(method);
	sd_bus_error_free(&error);
	sd_bus_message_unref(mes);

	if (!reply || strcmp(reply, "SUCCESS") != 0) {
		sd_bus_error_set_const(ret_error, "Forwarding Error", "Failed on file passing.");
		sd_bus_reply_method_return(m, "s", "FAILURE");
	}
	return sd_bus_reply_method_return(m, "s", "SUCCESS");
}

static int	app_down(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	const char	*app_name;
	serv_data	*data = (serv_data *)(userdata);
	int		r;

	r = sd_bus_message_read(m, "s", &app_name);
	if (r < 0) {
		fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
		return r;
	}

	if ((r = app_shutdown(data->db, app_name)) != 0) {
		sd_bus_error_set_const(ret_error, "SQL Error", "Failed on app status changing.");
		return sd_bus_reply_method_return(m, "s", "FAILURE");
	}

	return sd_bus_reply_method_return(m, "s", "SUCCESS");
}

static const sd_bus_vtable	manager_vtab[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("App_auth",	"ss",   "s", app_auth,	     SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("App_add_format", "ss",   "s", app_add_format, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("App_rm_format",  "ss",   "s", app_rm_format,  SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("App_down",       "s",    "s", app_down,       SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("Format_match",   "s",    "s", format_match,   SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("Share_file",     "ssss", "s", data_forward,   SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END
};

int	main(int argc, char **argv){
	sd_bus_slot	*slot	 = NULL;
	char		*err_msg = NULL;
	serv_data	data;
	int		r;

	r = sd_bus_open_user(&data.bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to user bus: %s\n", strerror(-r));
		goto finish;
	}

	r = sd_bus_add_object_vtable(data.bus,
                                    &slot,
                                    "/ru/unknown/AppSharing",
                                    "ru.unknown.AppSharing.AppManager",
                                    manager_vtab,
                                    NULL);
	if (r < 0) {
		fprintf(stderr, "Failed to issue method call: %s\n", strerror(-r));
		goto finish;
	}
	
	r = sd_bus_request_name(data.bus, "ru.unknown.AppSharing", 0);
	if (r < 0) {
		fprintf(stderr, "Failed to acquire service name: %s\n", strerror(-r));
		goto finish;
	}
	
	data.db = NULL;
	r = sqlite3_open("AppSharing.db", &data.db);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(data.db));
		goto finish;
	}

	/* Check for table existense and creation of it. */
	r = sqlite3_exec(data.db, "CREATE TABLE IF NOT EXISTS Apps(Name TEXT, Path TEXT, Status BOOL);", 0, 0, &err_msg);
	if (r != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		goto finish;
	}

	data.str = sdsempty();
//	How to pass userdata!
	sd_bus_slot_set_userdata(slot, (void *)&data);

	while (1) {
		r = sd_bus_process(data.bus, NULL);
		if (r < 0) {
			fprintf(stderr, "Failed to process bus: %s\n", strerror(-r));
			goto finish;
		}
		if (r > 0)
			continue;

		r = sd_bus_wait(data.bus, (uint64_t) -1);
		if (r < 0) {
			fprintf(stderr, "Failed to wait on bus: %s\n", strerror(-r));
			goto finish;
		}
	}

finish:
	sd_bus_slot_unref(slot);
	sd_bus_unref(data.bus);
	sqlite3_free(err_msg);
	sqlite3_close(data.db);
	sdsfree(data.str);

	return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
