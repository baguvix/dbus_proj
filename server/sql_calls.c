#include <sql_calls.h>
#include <sys/types.h>
#include <unistd.h>

int	is_registered(sqlite3 *db, const char *app_name) {
	sqlite3_stmt	*res = NULL;
	int		r;

	r = sqlite3_prepare_v2(db, "SELECT * FROM Apps WHERE Name = ?;", -1, &res, 0);
	if (r != SQLITE_OK)
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));

	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);
	
	r = sqlite3_step(res);
	
	sqlite3_finalize(res);
	if (r == SQLITE_ROW)
		return (1);
	return (0);
}

int	app_insert(sqlite3 *db, const char *app_name, const char *path) {
	sqlite3_stmt	*res = NULL;
	int		r;

	r = sqlite3_prepare_v2(db, "INSERT INTO Apps(Name, Path, Status) VALUES(?, ?, 1);", -1, &res, 0);
	if (r != SQLITE_OK)
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));

	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);
	sqlite3_bind_text(res, 2, path, -1, SQLITE_STATIC);

	r = sqlite3_step(res);
	
	sqlite3_finalize(res);
	if (r != SQLITE_DONE)
		return (r);
	return (0);
}

int	column_add(sqlite3 *db, const char *format) {
	sqlite3_stmt	*tmp = NULL;
	sds		s = sdsempty();
	int		r;

	s = sdscat(s, "ALTER TABLE Apps ADD COLUMN ");
	s = sdscat(s, format);
	s = sdscat(s, " BOOL;");

	r = sqlite3_prepare_v2(db, s, -1, &tmp, 0);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return (r);
	}

	r = sqlite3_step(tmp);

	sdsfree(s);
	sqlite3_finalize(tmp);
	if (r != SQLITE_DONE)
		return r;
	return (0);
}

int	format_add(sqlite3 *db, const char *app_name, const char *format) {
	sqlite3_stmt	*res = NULL;
	sds		s = sdsempty();
	int		r;

	s = sdscat(s, "UPDATE Apps SET ");
	s = sdscat(s, format);
	s = sdscat(s, "=1 WHERE Name=?;");

//	"UPDATE Apps SET (?)=1 WHERE Name=?;"
	r = sqlite3_prepare_v2(db, s, -1, &res, 0);
	if (r != SQLITE_OK) {
		if ((r = column_add(db, format)) != 0)
			return (r);
		sqlite3_finalize(res);
		r = sqlite3_prepare_v2(db, s, -1, &res, 0);
		if (r != SQLITE_OK) {
			fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
			return (r);
		}
	}

	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);

	r = sqlite3_step(res);

	sdsfree(s);
	sqlite3_finalize(res);
	if (r != SQLITE_DONE)
		return (r);
	return (0);
}

int	format_rm(sqlite3 *db, const char *app_name, const char *format) {
	sqlite3_stmt	*res = NULL;
	sds		s = sdsempty();
	int		r    = 0;

	s = sdscat(s, "UPDATE Apps SET ");
	s = sdscat(s, format);
	s = sdscat(s, "=0 WHERE Name=?;");

	r = sqlite3_prepare_v2(db, s, -1, &res, 0);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return (r);
	}
	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);

	r = sqlite3_step(res);

	sdsfree(s);
	sqlite3_finalize(res);
	if (r != SQLITE_DONE)
		return (r);
	return (0);
}

int	format_request(sqlite3 *db, const char *format, sds *apps) {
	sqlite3_stmt	*res = NULL;
	sds		s = sdsempty();
	int		r;

	sdsfree(*apps);
	*apps = sdsempty();

	s = sdscat(s, "SELECT * FROM Apps WHERE ");
	s = sdscat(s, format);
	s = sdscat(s, "=1;");

	r = sqlite3_prepare_v2(db, s, -1, &res, 0);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return (r);
	}

	r = 0;
	*apps = sdscat(*apps, "|");
	while (r != SQLITE_DONE) {
		r = sqlite3_step(res);
		if (r == SQLITE_ROW) {
			 *apps = sdscat(*apps, sqlite3_column_text(res, 0));
			 *apps = sdscat(*apps, "|");
		} else if (r != SQLITE_DONE) {
			fprintf(stderr, "SQL Error: %s\n", sqlite3_errmsg(db));
			return (r);
		}
	}

	sdsfree(s);
	sqlite3_finalize(res);
	if (r != SQLITE_DONE)
		return (r);
	return (0);
}

int	is_running(sqlite3 *db, const char *app_name) {
	sqlite3_stmt	*res = NULL;
	int		status;
	int		r;

	r = sqlite3_prepare_v2(db, "SELECT Status FROM Apps WHERE Name=?;", -1, &res, 0);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return (r);
	}

	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);

	r = sqlite3_step(res);

	if (r == SQLITE_ROW)
		status = sqlite3_column_int(res, 0);
	else
		return (r);

	sqlite3_finalize(res);
	return (status);
}

int	wake_up_call(sqlite3 *db, const char *app_name) {
	sqlite3_stmt	*res = NULL;
	const char	*path;
	pid_t		pid;
	int		r;

	r = sqlite3_prepare_v2(db, "SELECT Path FROM Apps WHERE Name=?;", -1, &res, 0);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return (r);
	}

	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);

	r = sqlite3_step(res);

	if (r == SQLITE_ROW)
		path = sqlite3_column_text(res, 0);
	else
		return (r);

	pid = fork();
	if (pid == -1) {	
		perror("fork");
		exit(EXIT_FAILURE);
	} else if (pid == 0) {
		int	ret;

		ret = execl(path, basename(path), NULL);
		if (ret == -1) {
			perror("execl");
			exit(EXIT_FAILURE);
		}
	}

	return (0);
}

int	app_shutdown(sqlite3 *db, const char *app_name) {
	sqlite3_stmt	*res = NULL;
	int		r;

	r = sqlite3_prepare_v2(db, "UPDATE APPS SET Status=0 WHERE Name=?;", -1, &res, 0);
	if (r != SQLITE_OK) {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return (r);
	}

	sqlite3_bind_text(res, 1, app_name, -1, SQLITE_STATIC);

	r = sqlite3_step(res);

	sqlite3_finalize(res);
	if (r != SQLITE_DONE)
		return (r);
	return (0);
}
